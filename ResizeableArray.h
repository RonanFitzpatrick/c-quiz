#pragma once
#ifndef RESIZEABLEARRAY_H_
#define RESIZEABLEARRAY_H_

#include <cstdlib>

template<class T>
class ResizeableArray {
private:
	int grow_size;


public:
	T* items;
	int nItems;
	int size;
	ResizeableArray()
	{
		grow_size = 2;
		items = new T[2];
		nItems = 0;
		size = 2;
	}

	ResizeableArray(int gs) {
		grow_size = gs;
		items = new T[gs];
		nItems = 0;
		size = gs;
	}
	void resize()
	{
		size += grow_size;
		T* temp = new T[size];

		for (int i = 0; i < nItems; i++)
		{
		
			temp[i] = items[i];
		}
		
		items = temp;
		

		//delete[] newItems

	}

	void push(const T& item) 
	{
		
		if (nItems == size)
		{
			resize();
		}
		
		items[nItems] = item;
		nItems++;
	}

	int length() 
	{
		return nItems;
	}

	T& getElement(int index) 
	{
		if (index < length() && index >= 0)
		{
			return items[index];
		}
		else
		{
			abort();
		}
	
	}
	void clear()
	{
		grow_size = grow_size;
		items = new T[grow_size];
		nItems = 0;
		size = grow_size;
	}

	T& operator[] (int index) {
		return getElement(index);
	}

	~ResizeableArray() {
		//delete[] items;
	}
};

#endif /* RESIZEABLEARRAY_H_ */