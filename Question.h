#include<string>
#include <regex>
#include<sstream>
using namespace std;


class Question
{
public:
	Question()
	{
		used = false;
	}
	Question(string typei,string questioni,string answeri)
	{
		used = false;
		type = typei;
		question = questioni;
		answer = answeri;
	}
	Question(string s)
	{
		used = false;
		//(.*?\,)(.*?\,)(.*?\,)(.*?\,)(.*?\,)(.*?\,)(.*)
		istringstream ss(s);
		string token;
		int count = 0;

		while (getline(ss, token, ',')) 
		{
			if (count == 0)
			{
				type = token;
			}
			else if (count == 1 || count > 2)
			{

				if (count > 2 && token.length() >1)
				{
					question += ",\n";
				}
				question += token + " ";
			}
			else
			{
				answer = token;
			}
			

			count++;
		}
		
	}

	string getType()
	{
		return type;
	}
	string getQuestion()
	{
		return question;
	}
	string getAnswer()
	{
		return answer;
	}
	bool getUsed()
	{
		return used;
	}
	void setUsed(bool x)
	{
		used = x;
	}

private:
	bool used;
	string type;
	string question;
	string answer;
};
