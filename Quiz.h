#include "Question.h"
#include "ResizeableArray.h"

using namespace std;

class Quiz
{
public:
	Quiz()
	{
		
	}
	Quiz(string s, ResizeableArray<Question>* qs)
	{
		
		title = s;
		questions = new ResizeableArray<Question>();
		for (int i = 0; i < qs->length(); i++)
		{
			questions->push(qs->getElement(i));
		}

	}
	string getTitle()
	{
		return title;
	}
	ResizeableArray<Question>* getQs()
	{
		return questions;
	}
	void reset()
	{
		for (int i = 0; i < questions->length(); i++)
		{
			questions->getElement(i).setUsed(false);
		}
	}

private:
	string title;
	ResizeableArray<Question>* questions;

};
